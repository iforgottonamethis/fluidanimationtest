//
//  main.m
//  FluidAnimationTest
//
//  Created by Umar on 12/12/15.
//  Copyright © 2015 Shazia Haroon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

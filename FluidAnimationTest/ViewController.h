//
//  ViewController.h
//  FluidAnimationTest
//
//  Created by Umar on 12/12/15.
//  Copyright © 2015 Shazia Haroon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>

@interface ViewController : UIViewController


@property(strong,nonatomic) CMMotionManager *motionManager;

@end


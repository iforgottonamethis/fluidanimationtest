//
//  ViewController.m
//  FluidAnimationTest
//
//  Created by Umar on 12/12/15.
//  Copyright © 2015 Shazia Haroon. All rights reserved.
//

#import "ViewController.h"
#import "BAFluidView.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    
}

-(void)viewDidLayoutSubviews{
    self.motionManager = [[CMMotionManager alloc] init];
    
    if (self.motionManager.deviceMotionAvailable) {
        self.motionManager.deviceMotionUpdateInterval = 0.3f;
        [self.motionManager startDeviceMotionUpdatesToQueue:[NSOperationQueue mainQueue]
                                                withHandler:^(CMDeviceMotion *data, NSError *error) {
                                                    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
                                                    NSDictionary* userInfo = [NSDictionary dictionaryWithObject:
                                                                              data forKey:@"data"];
                                                    [nc postNotificationName:kBAFluidViewCMMotionUpdate object:self userInfo:userInfo];
                                                }];
    }    // Do any additional setup after loading the view, typically from a nib.
    BAFluidView *fluidView = [[BAFluidView alloc] initWithFrame:self.view.frame startElevation:@0.5];
    fluidView.strokeColor = [UIColor clearColor];
    fluidView.fillColor = [UIColor greenColor];
    
    [fluidView keepStationary];
    [fluidView startAnimation];
    [fluidView startTiltAnimation];
    //    fluidView.fillDuration = 5.0;
    //    fluidView.fillRepeatCount = 1;
    //    fluidView.fillAutoReverse = NO;
    //        [fluidView fillTo:@1];
    
    
    
    UIImage *maskingImage = [UIImage imageNamed:@"Oval"];
    CALayer *maskingLayer = [CALayer layer];
    maskingLayer.frame = CGRectMake(CGRectGetMidX(fluidView.frame) - maskingImage.size.width/2, 180, maskingImage.size.width, maskingImage.size.height);
    [maskingLayer setContents:(id)[maskingImage CGImage]];
    [fluidView.layer setMask:maskingLayer];
    [self.view addSubview:fluidView];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
